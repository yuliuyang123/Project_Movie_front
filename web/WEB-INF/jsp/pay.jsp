<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<base href="<%=basePath %>"/>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="static/bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="static/bootstrap/js/bootstrap.min.js"></script>
    <link rel="icon" type="image/x-icon" href="static/images/logo.ico"/>
    <link rel="stylesheet" type="text/css" href="static/css/header.css">
    <link rel="stylesheet" type="text/css" href="static/css/pay.css">
    <link rel="stylesheet" type="text/css" href="static/css/footer.css">
    <script src="static/js/header.js" charset="utf-8"></script>
    <script src="static/js/Api.js"></script>
    <script src="static/layui/layui.js" charset="utf-8"></script>
    <link rel="stylesheet" href="static/layui/css/layui.css" media="all">
    <title>淘票票电影-支付</title>
</head>
<body>
<!-- ------------------------------------------------------------------- -->
<!-- 导航栏 -->
<jsp:include page="../../header.jsp"/>

<!-- 占位符 -->
<div id="div" style="margin-top: 170px;"></div>

<!-- 主体 -->
<div class="container">
    <div class="count-down-wrapper">
        <div class="count-down">
            <p class="time-left" id="CloseTime">
                请在<span class="minute" id="fen"></span>分钟<span class="second" id="miao"></span>秒内完成支付
            </p>
            <p class="tip">超时订单会自动取消，如遇支付问题，请联系管理员</p>
        </div>
    </div>

    <p class="warning">
        请仔细核对场次信息，出票后将<span class="attention">无法退票和改签</span>
    </p>

    <table class="order-table">
        <thead>
        <tr>
            <th>影片</th>
            <th>时间</th>
            <th>影厅</th>
            <th>座位</th>
        </tr>
        </thead>
        <tbody>
        <!-- 信息表 -->
        </tbody>
    </table>

    <div class="right">
        <div class="price-wrapper">
            <span>实际支付：</span>
            <span class="price"></span>
        </div>
        <div>
            <div id="zhifu" onclick="zhifu()" class="pay-btn">确认支付</div>
        </div>
    </div>
</div>
<div id="div1">
</div>
<!-- 脚 -->
<jsp:include page="../../footer.jsp"/>
<!-- ------------------------------------------------------------------- -->
</body>
</html>
<!-- ------------------------------------------------------------------- -->
<script>
    var clientHeight = document.documentElement.clientHeight;
    var oid =${param.ordeId};
    var oname;
    var money;
    var booking_time;
    $(function () {
        initHeader();
        initPay();

    })
    // // window.onload = function(){
    // //     initPay(); //支付
    // //     timeDown(); //计时器
    // // }
    //

    //初始化支付页面
    function initPay() {
        var orderTable = $(".order-table").find("tbody");
        $.ajax({
            type: 'post',
            url: "order/listByOrdeId",
            dataType: 'json',
            data: {
                ordeid: oid
            },
            success: function (obj) {
                console.info(obj)
                booking_time = obj.booking_time;
                orderTable.append(
                    "<tr>" +
                    "<td class=\"movie-name\">" + obj.ticketSales.film.chinese_name + "</td>" +
                    "<td class=\"showtime\">" + obj.ticketSales.opening_time + "</td>" +
                    "<td class=\"cinema-name\">" + obj.ticketSales.hall.hname + "</td>" +
                    "<td>" +
                    "<span class=\"hall\">" + obj.buy_seats + "</span>" +
                    "<div class=\"seats\">" +
                    "<div class=\"choiceseat\" style=\"display: block;\">" +
                    "</div>" +
                    "</div>" +
                    "</td>" +
                    "<tr>"
                );
                $(".price").html(obj.price);
                oname = obj.ticketSales.film.chinese_name;
                money = obj.price;
                jishiqi()
            }
        });

    }


    function zhifu() {
        $.post("order/pay", {oid: oid, oname: oname, money: money}, function (data) {
            console.info(data);
            if (data.success == 1) {
                $("#div1").append(data.target);
            } else {
                alert(data.error);
            }
        })
    }

    //计时器
    function jishiqi() {
        var StartTime = new Date(booking_time);
        var EndTime = StartTime.getTime() + 60 * 1000;//约定订购时间15天后关闭交易
        EndTime = new Date(EndTime);
        var nMS, nM, nS;
        setInterval(function () {
            var oDate = new Date();
            nMS = EndTime - oDate;
            nM = Math.floor(nMS / (1000 * 60)) % 60;
            nS = Math.floor(nMS / 1000) % 60;
            document.getElementById("fen").innerHTML = (nM);
            document.getElementById("miao").innerHTML = (nS);
            if (nM < 0 || nS < 0) {
                document.getElementById("CloseTime").innerHTML = "交易因超过1分钟,订单已取消";
                $.post("order/updateState", {oid: oid}, function (data) {
                    if (data.success) {
                        layer.msg("订单已取消")
                    } else {
                        layer.msg(data.error)
                    }
                });
                clearInterval();
            }
        }, 1000);
    }


</script>

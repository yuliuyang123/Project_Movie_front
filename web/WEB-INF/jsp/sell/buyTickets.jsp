<%--
  Created by IntelliJ IDEA.
  User: YuLiuYang
  Date: 2022/3/24
  Time: 18:30
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<base href="<%=basePath %>" />
<script src="static/bootstrap/js/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="static/css/header.css">
<link rel="stylesheet" type="text/css" href="static/css/buySeat.css">
<link rel="stylesheet" type="text/css" href="static/css/footer.css">
<script src="static/js/header.js" charset="utf-8"></script>
<script src="static/js/Api.js"></script>

<script src="static/layui/layui.js" charset="utf-8"></script>
<link rel="stylesheet" href="static/layui/css/layui.css" media="all">
<html>
<head>
    <title>Title</title>
</head>
<body>
<!-- ------------------------------------------------------------------- -->
<!-- 导航栏 -->
<jsp:include page="../../../header.jsp"/>
<!-- 占位符 -->
<div style="margin-top: 90px;"></div>
<div class="container">
    <div class="main">
        <!-- 主页 -->
        <div class="hall">
            <div class="seat-example">
                <div class="selectable-example example">
                    <span>可选座位</span>
                </div>
                <div class="sold-example example">
                    <span>已售座位</span>
                </div>
                <div class="selected-example example">
                    <span>已选座位</span>
                </div>

            </div>
            <div class="seats-block">
                <div class="row-id-container">
                </div>
                <div class="seats-container">
                    <div class="screen-container">
                        <div class="screen">银幕中央</div>
                        <div class="c-screen-line"></div>
                    </div>
                    <div class="seats-wrapper">
                    </div>
                </div>
            </div>
        </div>
        <!-- 侧页 -->
        <div class="side">
            <!-- 电影 -->
            <div class="movie-info clearfix">
            </div>
            <!-- 场次 -->
            <div class="show-info">

            </div>
            <div class="ticket-info">
                <div class="no-ticket" style="display: block;">
                    <p class="buy-limit">座位：一次最多选4个座位</p>
                </div>
                <div class="has-ticket" style="display: none;">
                    <span class="text">座位：</span>
                    <div class="ticket-container">
                    </div>
                </div>
                <div class="total-price">
                    <span>总价：</span>
                    <span class="price"></span>
                </div>
            </div>
            <div class="confirm-order">
                <div class="email">
                    <span>用户：${sessionScope.SESSION_USERS.luname}</span>
                </div>
                <div class="confirm-btn disable" id="Confirm">确认选座</div>
            </div>
        </div>
    </div>
</div>

</html>
<!-- 脚 -->
<jsp:include page="../../../footer.jsp"/>
<script>
    window.onload = function(){
        initHeader();
    }
    layui.use(['form', 'table', 'tree'], function () {
        var form = layui.form,
            layer = layui.layer,
            table = layui.table,
            tree = layui.tree,
            $ = layui.$;
        $(function () {
            initInformation(); //信息
        });
        //选中场次id
        var tid = ${param.tid};
        var fid=${param.fid};
        var luid = ${sessionScope.SESSION_USERS.lid};
        var SeatMax = 0;
        var PriceTemp = 1;
        var price;
        var buySeats = "";
        $(".seats-wrapper").on('click', '.seat', function () {
            var row = $(this).attr('row');
            var col = $(this).attr('col');
            var Ticket = $(".ticket-container");
            var NoTicket = $(".no-ticket")[0];
            var HasTicket = $(".has-ticket")[0];
            var ConfirmBtn = $(".confirm-btn")[0];
            var TicketPrice = $(".price");
            NoTicket.style.display = "none";
            HasTicket.style.display = "block";
            // alert(SeatMax,"max")
            //
            var rclass = $(this).attr('class');
            if((SeatMax>5)&&(rclass == "seat")){
                  layer.msg("一次只能选6个座位")
                  return;
              }else {
            if ($(this).is('.selectable')) {
                $(this).removeClass('selectable');
                SeatMax--;
                $("[data-index=" + row + col + "]").remove();
                price = PriceTemp * SeatMax
                TicketPrice.text("￥" + price);
            } else {
                $(this).addClass('selectable');
                SeatMax++;
                Ticket.append("<span class=\"ticket\" data-index=\"" + row + col + "\">" + row + col + "</span>");
                price = PriceTemp * SeatMax
                TicketPrice.text("￥" + price);
            }
            if (SeatMax == 0) {
                NoTicket.style.display = "block";
                HasTicket.style.display = "none";
                ConfirmBtn.className = "confirm-btn disable";
            } else {
                NoTicket.style.display = "none";
                HasTicket.style.display = "block";
                ConfirmBtn.className = "confirm-btn";
            }
            }
        });

        function initSeat(hang, lie) {
            var flag;
            var $rowIdContainer = $(".row-id-container");
            for (var i = 1; i <= hang; i++) {
                $('<span class="row-id">').text(i).appendTo($rowIdContainer);
                var $seat = $(".seats-wrapper");
                var $row = $('<div class="row">').appendTo($seat);
                for (var j = 1; j <= lie; j++) {
                    if (buySeats.length == 0) {
                        var $col = $('<span class="seat">').appendTo($row);
                        $col.attr('row', i + "排").attr('col', j + "座")
                    } else {
                        flag = 0;
                        for (var p = 0; p < buySeats.length; p++) {
                            if (buySeats[p] == (i + "排" + j + "座")) {
                                flag = 1;
                            }
                        }
                        if (flag == 1) {
                            var $col = $('<span class="seat sold">').appendTo($row);
                            $col.attr('row', i + "排").attr('col', j + "座")
                        } else {
                            var $col = $('<span class="seat">').appendTo($row);
                            $col.attr('row', i + "排").attr('col', j + "座")
                        }
                    }
                }
            }
        }

        //点击具体座位事件

        /**
         * 初始化表单，要加上，不然刷新部分组件可能会不加载
         */
        form.render();

        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;



        function initInformation() {
            var movieInfo = $(".movie-info.clearfix");
            var showInfo = $(".show-info");
            $.ajax({
                type: 'post',
                url: "sell/findTicketSalesByTId?tid=" + tid + "",
                dataType: 'json',
                success: function (obj) {
                    var TempLength = obj.orderList.length;
                    var buySeat = "";
                    for (var i = 0; i < TempLength; i++) {
                        buySeat += obj.orderList[i].buy_seats
                    }
                    buySeats = buySeat.split(",")
                    PriceTemp = obj.price;
                    hid = obj.hall.hid;
                    movieInfo.append(
                        "<div class='poster'>" +
                        "<img src= \"http://localhost:8081/img/" + obj.film.imgUrl + "\">" +
                        "</div>" +
                        "<div class=\"content\">" +
                        "<p class=\"name text-ellipsis\">" + obj.film.chinese_name + "</p>" +
                        "<div class=\"info-item\">" +
                        "<span>类型：</span>" +
                        "<span class=\"value\">" + obj.film.ftype.name + "</span>" +
                        "</div>" +
                        "<div class=\"info-item\">" +
                        "<span>时长：</span>" +
                        "<span class=\"value\">\"" + obj.film.duration + "分钟\"</span>" +
                        "</div>" +
                        "</div>"
                    );
                    showInfo.append(
                        "<div class=\"info-item\">" +
                        "<span>影院：</span>" +
                        "<span class=\"value\">横店影城</span>" +
                        "</div>" +
                        "<div class=\"info-item\">" +
                        "<span>影厅：</span>" +
                        "<span class=\"value\">" + obj.hall.hname + "</span>" +
                        "</div>" +
                        "<div class=\"info-item\">" +
                        "<span>版本：</span>" +
                        "<span class=\"value\">" + obj.film.country + "</span>" +
                        "</div>" +
                        "<div class=\"info-item\">" +
                        "<span>场次：</span>" +
                        "<span class=\"value\">" + obj.opening_time + "</span>" +
                        "</div>" +
                        "<div class=\"info-item\">" +
                        "<span>票价：</span>" +
                        "<span class=\"value\">￥" + obj.price + "/张</span>" +
                        "</div>"
                    );
                    initSeat(obj.hall.hang, obj.hall.lie);
                }
            });
        }

        //确定选座
        $( '#Confirm').bind('click', function () {
            //获取选中的座位
            var srt = ""
            //座位数
            var seat = ""
            var ordeId = "";   //定义用户编号
            for (var i = 0; i < 4; i++) //4位随机数，用以加在时间戳后面。
            {
                ordeId += Math.floor(Math.random() * 10);
            }
            ordeId = new Date().getTime() + ordeId;
            var numseat = 0;
            $('span[class="ticket"]').each(function () {
                srt = $(this).attr('data-index') + ",";
                seat += srt;
                numseat++;
            });
            $.post("order/save", {
                tid: tid,
                buy_seats: seat,
                price: price,
                luid: luid,
                fid: fid,
                ordeId:ordeId,
                numseat: numseat
            }, function (data) {
                if (data.success) {
                    // 关闭弹出层
                    layer.closeAll();
                    location.href="order/toList?ordeId="+ordeId+"";
                    //刷新界面
                    table.reload("currentTableId");
                } else {
                    layer.msg(data.error);
                }
            }, "json")
        });
    });
</script>


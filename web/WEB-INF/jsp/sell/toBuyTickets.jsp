<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<base href="<%=basePath %>" />
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="static/bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="static/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="static/css/header.css">
    <link rel="stylesheet" type="text/css" href="static/css/main.css">
    <link rel="stylesheet" type="text/css" href="static/css/footer.css">
    <link rel="stylesheet" type="text/css" href="static/css/buyTickets.css">
    <link rel="stylesheet" type="text/css" href="static/css/movieDetail.css">
    <link rel="stylesheet" type="text/css" href="static/css/selectSeat.css">
    <script src="static/js/header.js" charset="utf-8"></script>
    <script src="static/js/Api.js"></script>

    <script src="static/layui/layui.js" charset="utf-8"></script>
    <link rel="stylesheet" href="static/layui/css/layui.css" media="all">
    <title>淘票票电影-选影院</title>
</head>
<body>
<!-- ------------------------------------------------------------------- -->
<!-- 导航栏 -->
<jsp:include page="../../../header.jsp"/>

<!-- 占位符 -->
<div style="margin-top: 80px;"></div>

<!-- 巨幕 -->
<div class="banner2">
    <div class="wrapper clearfix">
        <div class="celeInfo-left">
            <div class="avatar-shadow">
                <!-- 图片 -->
            </div>
        </div>

        <div class="celeInfo-right clearfix">
            <div class="movie-brief-container">
                <!-- 上 -->
            </div>
            <div class="action-buyBtn">
            </div>

            <div class="movie-stats-container">
                <div class="movie-index">
                    <p class="movie-index-title">累计票房</p>
                    <div class="movie-index-content box stonefont-num">
                        <!-- 票房数 -->
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- 占位符 -->
<div style="margin-top: 50px;"></div>

<!-- 主体 -->
<div class="main">
    <div class="main-inner main-buyticket">
        <!-- 标签 -->

        <!-- 列表 -->
        <div class="cinemas-list">
            <h2 class="cinemas-list-header">场次列表</h2>
        </div>
        <!-- 电影信息 -->
        <div class="movie-info">
        </div>

        <div class="show-list active" data-index="0">
            <!-- 电影信息 -->
            <div class="movie-info">
            </div>
            <!-- 场次列表 -->
            <div class="plist-container active">
                <table class="plist">
                    <thead>
                    <tr>
                        <th>放映时间</th>
                        <th>语言版本</th>
                        <th>放映厅</th>
                        <th>售价（元）</th>
                        <th>选座购票</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
    </div>
</div>
</body>
</html>
<!-- 脚 -->
<jsp:include page="../../../footer.jsp"/>

<!-- ------------------------------------------------------------------- -->
<script>
    var fid=${param.fid};
    $(function(){
        initBanner(); //巨幕
        initHeader();
    });



    //初始化巨幕
    function initBanner(){
        var avatar = $(".avatar-shadow");
        var movieBriefContainer = $(".movie-brief-container");
        var infoNum = $(".info-num");
        var scoreNum = $(".score-num");
        var stonefontNum = $(".stonefont-num");
        var actionBuyBtn = $(".action-buyBtn");
        var StonefontTemp;

        $.ajax({
            type:'post',
            url: "sell/listData",
            dataType:'json',
            data: {
                fid: fid
            },
            success:function (obj) {
                StonefontTemp = "136";
                StonefontTemp += "万";
                //console.info(obj)
                avatar.append("<img class=\"avatar\" src= \"http://localhost:8081/img/" + obj.film.imgUrl + "\">");
                movieBriefContainer.append(
                    "<h3 class=\"name\">" + obj.film.chinese_name + "</h3>" +
                    "<div class=\"ename ellipsis\">" + obj.film.english_name + "</div>" +
                    "<ul>" +
                    "<li class=\"ellipsis\">" + obj.film.ftype.name + "</li>" +
                    "<li class=\"ellipsis\">" + obj.film.duration + "分钟 / " + obj.film.country + "</li>" +
                    "<li class=\"ellipsis\">" + obj.break_time + "</li>" +
                    "<ul>");
                stonefontNum.append("<span class=\"stonefont\">" + StonefontTemp + "</span>");
                actionBuyBtn.append("<a class=\"btn buy\" id='pinglun' data-act=\"more-detail-click\">评论</a>");
            }
        });
    }

    $.ajax({
        type:'post',
        url:"sell/listChangci",
        dataType:'json',
        data: {
           fid:fid
        },
        success:function (obj) {
            console.info(obj)
            var plist = $(".plist").find("tbody");
            for(var i = 0;i < obj.length;i++){
                plist.append(
                    "<tr class=\"\">" +
                    "<td> <span class=\"begin-time\">"+ obj[i].break_time +"</span> <br> </td>" +
                    "<td> <span class=\"lang\">" + obj[i].film.country +"</span> </td>" +
                    "<td> <span class=\"hall\">" + obj[i].hall.hname + "</span> </td>" +
                    "<td> <span class=\"sell-price\"> <span class=\"stonefont\">" + obj[i].price + "</span> </span> </td>" +
                    "<td> <a  href=\"javascript:void(0);\"  id='goupiao' value=\""+ obj[i].tid +"\" class=\"buy-btn normal\">选座购票</a> </td>" +
                    "</tr>"
                );
            }

        }
    });
    $(document).on('click', "#goupiao",function () {
        var tid = $(this).attr('value');
        location.href="sell/buyTickets?tid="+tid+"&fid="+fid+"";
    })
    $(document).on('click', "#pinglun",function () {
        location.href="pinglun/toList?fid="+fid+"";
    })


</script>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<base href="<%=basePath %>"/>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="static/bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="static/bootstrap/js/bootstrap.min.js"></script>

    <link rel="icon" type="image/x-icon" href="static/images/logo.ico"/>
    <link rel="stylesheet" type="text/css" href="static/css/header.css">
    <link rel="stylesheet" type="text/css" href="static/css/main.css">
    <link rel="stylesheet" type="text/css" href="static/css/footer.css">
    <link rel="stylesheet" type="text/css" href="static/css/buyTickets.css">
    <link rel="stylesheet" type="text/css" href="static/css/movieDetail.css">
    <script src="static/js/header.js" charset="utf-8"></script>
    <script src="static/js/Api.js"></script>

    <script src="static/layui/layui.js" charset="utf-8"></script>
    <link rel="stylesheet" href="static/layui/css/layui.css" media="all">
    <title>淘票票电影-详细</title>
</head>
<body>
<!-- ------------------------------------------------------------------- -->
<!-- 导航栏 -->
<jsp:include page="../../header.jsp"/>

<!-- 占位符 -->
<div style="margin-top: 80px;"></div>

<!-- 巨幕 -->
<div class="banner2">
    <div class="wrapper clearfix">
        <div class="celeInfo-left">
            <div class="avatar-shadow">
                <!-- 图片 -->
            </div>
        </div>

        <div class="celeInfo-right clearfix">
            <div class="movie-brief-container">
                <!-- 上 -->
            </div>
            <div class="action-buyBtn">
                <div class="action clearfix" data-val="{movieid:42964}">

                </div>
            </div>

            <div class="movie-stats-container">
                <div class="movie-index">
                    <p class="movie-index-title">累计票房</p>
                    <div class="movie-index-content box stonefont-num">
                        <!-- 票房数 -->
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- 占位符 -->
<div style="margin-top: 50px;"></div>

<!-- 主体 -->
<div class="main">
    <div class="main-inner main-detail">
        <div class="main-content">
            <div class="tab-container">


                <div class="tab-content-container">
                    <div class="tab-desc tab-content active" data-val="{tabtype:'desc'}">
                        <!-- 评论 -->
                        <div class="module">
                            <div class="mod-title">
                                <h3>热门短评</h3>
                            </div>

                            <div class="mod-content">
                                <div class="comment-list-container" data-hot="10">
                                    <ul>
                                    </ul>
                                </div>
                                <a class="comment-entry" data-act="comment-no-content-click" onclick="writeComment()">写短评</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 脚 -->
<jsp:include page="../../footer.jsp"/>

<!-- ------------------------------------------------------------------- -->
<script>
    var clientHeight = document.documentElement.clientHeight;
    var fid =${param.fid};
    var luid = "${sessionScope.SESSION_USERS.lid}";
    var falg;
    if(luid===""){
        falg =0;
    }else {
        falg=1
    }
    window.onload = function () {
        initHeader();
        initBanner(); //巨幕
        init_comment(); //评论
        initHtml();
    }

    //初始化HTML
    function initHtml() {

        WriteCommentHtml =
            "<div id='test5'></div>"+
            "<h3 class=\"commenttitle\">发表评论</h3>" +
            "<div class=\"layui-form-item\">" +
            "<label class=\"layui-form-label commentcontenttext\">评论内容</label>" +
            "<div class=\"layui-input-block commentcontent\">" +
            "<textarea id=\"comment_content_write\" style=\"height:150px;\" placeholder=\"请输入评论内容\" autocomplete=\"off\" class=\"layui-textarea\" name=\"desc\" class=\"layui-input\"></textarea>" +
            "</div>" +
            "</div>"
        ;
    }

    //验证用户身份
    function init_comment() {
        var commentListContainer = $(".comment-list-container").find("ul")
        $.ajax({
            type: 'post',
            url: "comments/listData",
            dataType: 'json',
            data: {
                fid: fid
            },
            success: function (obj) {
                console.info(obj)
                for (var i = 0; i < obj.length; i++) {
                    commentListContainer.append(
                        "<li class=\"comment-container\">" +
                        "<div class=\"portrait-container\">" +
                        "<div class=\"portrait\">" +
                        "<img src=\"http://localhost:8081/img/" + obj[i].leadingUser.picture + "\" alt=\"\">" +
                        "</div>" +
                        "<i class=\"level-4-icon\"></i>" +
                        "</div>" +
                        "<div class=\"main2\">" +
                        "<div class=\"main2-header clearfix\">" +
                        "<div class=\"user\">" +
                        "<span class=\"name\">" + obj[i].leadingUser.luname + "</span>	" +
                        "</div>" +
                        "<div class=\"time\" title=\"2018-11-16 12:06:10\">" +
                        "<span title=\"2018-11-16 12:06:10\">" + obj[i].create_time + "</span>" +
                        "</div>" +
                        "<div class=\"approve\" data-id=\"1044884745\">" +
                        "</div>" +
                        "</div>" +
                        "<div class=\"comment-content\"> " +
                        obj[i].content +
                        "</div>" +
                        "</div>" +
                        "</ul>"
                    );
                }
            }
        });
    }

    //初始化巨幕
    function initBanner() {
        var avatar = $(".avatar-shadow");
        var movieBriefContainer = $(".movie-brief-container");
        var stonefontNum = $(".stonefont-num");
        var actionBuyBtn = $(".action-buyBtn");
        var StonefontTemp;

        $.ajax({
            type: 'post',
            url: "sell/listData",
            dataType: 'json',
            data: {
                fid: fid
            },
            success: function (obj) {
                StonefontTemp = "136";
                StonefontTemp += "万";
                //console.info(obj)
                avatar.append("<img class=\"avatar\" src= \"http://localhost:8081/img/" + obj.film.imgUrl + "\">");
                movieBriefContainer.append(
                    "<h3 class=\"name\">" + obj.film.chinese_name + "</h3>" +
                    "<div class=\"ename ellipsis\">" + obj.film.english_name + "</div>" +
                    "<ul>" +
                    "<li class=\"ellipsis\">" + obj.film.ftype.name + "</li>" +
                    "<li class=\"ellipsis\">" + obj.film.duration + "分钟 / " + obj.film.country + "</li>" +
                    "<li class=\"ellipsis\">" + obj.break_time + "</li>" +
                    "<ul>");
                stonefontNum.append("<span class=\"stonefont\">" + StonefontTemp + "</span>");
                actionBuyBtn.append("<a class=\"btn buy\" id='goupiao' data-act=\"more-detail-click\">去购票</a>");
            }
        });
    }

    $(document).on('click', "#goupiao", function () {
        window.history.go(-1)
    });

    //写评论
    function writeComment() {
           if(falg==1) {
               layui.use(['laypage', 'layer', 'table','rate'], function () {
                   var laypage = layui.laypage;
                   var layer = layui.layer;
                   var table = layui.table;
                   var rate = layui.rate;
                   //写评论

                   rate.render({
                       elem: '#test5'
                       , value: 3
                       , text: true
                       , setText: function (value) { //自定义文本的回调
                           var arrs = {
                               '1': '极差'
                               , '2': '差'
                               , '3': '中等'
                               , '4': '好'
                               , '5': '极好'
                           };
                           this.span.text(arrs[value] || (value + "星"));
                       }
                   });

                   layer.open({
                       type: 1
                       , title: "评论" //不显示标题栏
                       , closeBtn: false
                       , area: '430px;'
                       , shade: 0.8
                       , offset: clientHeight / 20
                       , id: 'LAY_layuipro' //设定一个id，防止重复弹出
                       , btn: ['发表', '取消']
                       , yes: function () {
                           //评论的内容
                           var comment_content = $('#comment_content_write').val();
                           if (comment_content == "") {
                               layer.alert('评论内容不能空，评论失败！', {icon: 0, offset: clientHeight / 5},
                                   function () {
                                       layer.close(layer.index);
                                   }
                               );
                           } else {
                               if (comment_content.length > 150) {
                                   layer.alert('字数超过150个，评论失败！', {icon: 0, offset: clientHeight / 5},
                                       function () {
                                           layer.close(layer.index);
                                       }
                                   );
                               } else {
                                   console.log(comment_content);
                                   $.ajax({
                                       type: 'post',
                                       url: "comments/save",
                                       dataType: 'json',
                                       data: {
                                           luid:luid,
                                           fid: fid,
                                           content:comment_content
                                       },
                                       success: function (obj) {
                                           if (obj.success) {
                                               layer.alert('评价成功！', {icon: 0, offset: clientHeight / 5},
                                                   function () {
                                                       layer.closeAll();
                                                       location.reload();
                                                   }
                                               );
                                           } else {
                                               layer.alert(obj.msg, {icon: 0, offset: clientHeight / 5},
                                                   function () {
                                                       layer.closeAll();
                                                   }
                                               );
                                           }
                                       }
                                   });

                               }
                           }
                       }
                       , btnAlign: 'c movie-last'
                       , moveType: 0 //拖拽模式，0或者1
                       , content: WriteCommentHtml
                       // ,success: function(layero){
                       //     $('#user_name_write').val(user_name);
                       // }
                   });
               });
           }else {
               layer.msg("请登录后评论")
           }
    }


</script>
<!-- ------------------------------------------------------------------- -->

</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<base href="<%=basePath %>" />

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="static/bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="static/bootstrap/js/bootstrap.min.js"></script>
    <link rel="icon" type="image/x-icon" href="static/images/logo.ico"/>
    <link rel="stylesheet" type="text/css" href="static/css/header.css">
    <link rel="stylesheet" type="text/css" href="static/css/main.css">
    <link rel="stylesheet" type="text/css" href="static/css/main2.css">
    <link rel="stylesheet" type="text/css" href="static/css/footer.css">
    <script src="static/js/header.js" charset="utf-8"></script>
    <script src="static/js/Api.js"></script>

    <script src="static/layui/layui.js" charset="utf-8"></script>
    <link rel="stylesheet" href="static/layui/css/layui.css" media="all">
    <title>淘票票电影-首页</title>
</head>
<body>
    <!-- ------------------------------------------------------------------- -->
    <!-- 导航栏 -->
    <jsp:include page="../../header.jsp"/>

    <!-- 占位符 -->
    <div style="margin-top: 100px;"></div>

    <!-- 主体 -->
    <div class="main">
        <div class="main-inner main-page">
            <div class="layui-carousel" id="test3" lay-filter="test4">
                <div carousel-item="">
                    <div>
                        <img src="static/images/lunbo8.jpg">
                    </div>
                    <div>
                        <img src="static/images/pic3.jpg">
                    </div>
                    <div>
                        <img src="static/images/pic7.jpg">
                    </div>
                    <div>
                        <img src="static/images/pic8.jpg">
                    </div>
                    <div>
                        <img src="static/images/lunbo6.jpg">
                    </div>
                </div>
            </div> 
            <div class="movie-grid" style="width: 1272px;">

                <div class="panel-header">
                    <%--<span class="panel-more">--%>
                        <%--<a href="./movieList.jsp" class="textcolor_red" data-act="all-playingMovie-click">--%>
                            <%--<span>全部</span>--%>
                        <%--</a>--%>
                        <%--<span class="panel-arrow panel-arrow-red"></span>--%>
                    <%--</span>--%>
                    <span class="panel-title hot-title">
                    </span>
                </div>
                <div class="panel-content">
                    <ul class="movie-list movie-hot">
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <!-- 脚 -->
    <jsp:include page="../../footer.jsp"/>
    
    <!-- ------------------------------------------------------------------- -->
    <script>
        var clientHeight = document.documentElement.clientHeight;

        window.onload = function(){
            initHostMovie(); //初始化正在热映和即将上映电影
            //initBoxOffice(); //初始化总体票房
            initHeader();

        }

        //图片轮播
        layui.use(['carousel', 'form'], function(){
            var carousel = layui.carousel, form = layui.form;
            carousel.render({
                elem: '#test3'
                ,width: '100%'
                ,height: '368px'
                ,interval: 5000
            });
        });
        
        //初始化正在热映和即将上映电影
        function initHostMovie(){
            var MoiveLiHot = $(".movie-hot");
            var MoiveLiOn = $(".movie-on");
            var htmlHot,htmlOn;
            var ListLength;
            var notice, sale;
            var HotNum = $(".hot-title");
            var OnNum = $(".on-title");
            var TempName;

            $.ajax({
                type:'post',
                url: "sell/listAll",
                dataType:'json',
                data: {},
                success:function (obj) {
                    console.log(obj);
                    HotNum.append("<span class=\"textcolor_red\">正在热映（" + obj.length + "）</span>");
                    if(obj.length<8){
                        ListLength = obj.length;
                    }
                    else{
                        ListLength = 8;
                    }
                    for (var i = 0; i < obj.length; i++) {
                        htmlHot =
                            "<li>" +
                            "<div class=\"movie-item\">" +
                            "<a   target=\"_blank\" data-act=\"playingMovie-click\" data-val=\"" + obj[i].film.id + "\">" +
                            "<div class=\"movie-poster\" style=\"cursor:default;\">" +
                            "<img id=\"moive_picture\" src= \"http://localhost:8081/img/" + obj[i].film.imgUrl + "\">" + "<div class=\"movie-overlay movie-overlay-bg\"><div class=\"movie-info\">" +
                            "<div class=\"movie-score\"><i id=\"moive_score\" class=\"integer\"> 10 </i></div>" +
                            "<div class=\"movie-title movie-title-padding\" title=\"" + obj[i].film.chinese_name + "\">" + obj[i].film.chinese_name + "</div>" +
                            "</div>" +
                            "</div>" +
                            "</div>" +
                            "</a>" +
                            "<div class=\"movie-detail movie-detail-strong movie-sale\">" +
                            "<a id='goupiao' value='"+obj[i].film.id+"'class=\"active\" target=\"_blank\" data-act=\"salePlayingMovie-click\" data-val=\"{movieid:42964}\">购 票</a>" +
                            "</div>" +
                            "</div>" +
                            "</li>";
                        MoiveLiHot.append(htmlHot);
                    }

                    $(document).on('click', "#goupiao",function () {
                        var fid = $(this).attr('value');
                        console.info(fid);
                        location.href="sell/toBuyTickets?fid="+fid+"";
                    })

                }
            });
        }

        //初始化总体票房
        function initBoxOffice(obj){
            console.log(obj);
            var TempLength;
            if(obj.sort.length>9){
                TempLength = 9;
            }
            else{
                TempLength = obj.sort.length;
            }
            var BoxOffice = $(".boxOffice");
            for(var i=0;i<TempLength;i++){
                if(i==0){
                    BoxOffice.append(
                        "<li class=\"ranking-item ranking-top ranking-index-1\">" +
                            "<a href=\"./movieDetail.jsp?movie_id=" + obj.sort[i].movie_id + "\" target=\"_blank\">" +
                                "<div class=\"ranking-top-left\">" +
                                    "<i class=\"ranking-top-icon\"></i>" +
                                    "<img class=\"ranking-imgs  default-img\" src=\"" + obj.sort[i].movie_picture + "\">" +
                                "</div>" +
                                "<div class=\"ranking-top-right\">" +
                                    "<div class=\"ranking-top-right-main\">" +
                                        "<span class=\"ranking-top-moive-name\">" + obj.sort[i].movie_cn_name + "</span>" +
                                        "<p class=\"ranking-top-wish\">" +
                                            "<span class=\"stonefont\">" + obj.sort[i].movie_boxOffice + "</span>万" +
                                        "</p>" +
                                    "</div>" +
                                "</div>" +
                            "</a>" +
                        "</li>"
                    );
                }
                else{
                    BoxOffice.append(
                        "<li class=\"ranking-item ranking-index-4\">" +
                            "<a href=\"./movieDetail.jsp?movie_id=" + obj.sort[i].movie_id + "\" target=\"_blank\">" +
                                "<span class=\"normal-link\">" +
                                    "<i class=\"ranking-index ranking-index-"+(i+1)+"\">" + (i+1) + "</i>" +
                                    "<span class=\"ranking-movie-name\">" + obj.sort[i].movie_cn_name + "</span>" +
                                    "<span class=\"ranking-num-info\">" +
                                        "<span class=\"stonefont\">" + obj.sort[i].movie_boxOffice + "</span>万" +
                                    "</span>" +
                                "</span>" +
                            "</a>" +
                        "</li>"
                    );
                }
            }
        }


    </script>
    <!-- ------------------------------------------------------------------- -->
</body>
</html>
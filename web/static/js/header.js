﻿//初始化
var lid;
var password;
function initHeader() {
    var LayuiNavMore = $(".header-li");
    layui.use('element', function () {
        var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
        //监听导航点击
        element.on('nav(demo)', function (elem) {
            //console.log(elem)
            layer.msg(elem.text());
        });
    });
    $.ajax({
        type: 'post',
        url: "session",
        dataType: 'json',
        data: {},
        async: false,
        // var b = $.isEmptyObject(data);
        // alert(b);
        success: function (obj) {
            if (obj.success==0) {
                LayuiNavMore.append(
                    "<a href=\"javascript:;\" style=\"padding: 19px;height: 42px; width: 42px;\"><img src=\"static/images/head.jpg\" class=\"layui-nav-img\"></a>" +
                    "<dl class=\"layui-nav-child nav-image\">" +
                    "<dd><a href=\"./login.jsp\">登录</a></dd>" +
                    "</dl>"
                );
            } else {
                lid =obj.data[0].lid
                password =obj.data[0].password
                console.info(obj.data[0].lid)
                LayuiNavMore.append(
                    "<a href=\"javascript:;\" style=\"padding: 19px;height: 42px; width: 42px;\"><img src=\"http://localhost:8081/img/"+obj.data[0].picture+"\" class=\"layui-nav-img\"></a>" +
                    "<dl class=\"layui-nav-child nav-image\">" +
                    "<dd><a href=\"javascript:void(0);\" onclick=\"mycenter()\">我的订单</a></dd>" +
                    "<hr/>" +
                    "<dd><a href=\"javascript:void(0);\" onclick=\"myinformation()\">基本信息</a></dd>" +
                    "<hr/>" +
                    "<dd><a  href=\"javascript:void(0);\" onclick=\"ReLogin()\" style=\"text-decoration: none; cursor: pointer;\">注销</a></dd>" +
                    "<hr/>" +
                    "</dl>"
                );
            }
        }
    })

}

function mycenter() {
    localStorage.setItem("usercardId", 0);
    location.href = "center/toList?lid="+lid+"&password="+password+"";
}

function myinformation() {
    localStorage.setItem("usercardId", 1);
    location.href = "center/toList?lid="+lid+"&password="+password+"";
}

//注销
function ReLogin() {
    layui.use(['layer'], function () {
        var layer = layui.layer;
        layer.alert('确认要注销吗？', {icon: 0, offset: clientHeight / 10},
            function () {
                $.ajax({
                    type: 'post',
                    url: "uotSession",
                    dataType: 'json',
                    data: {},
                    success: function (obj) {
                        console.info(obj + "3")
                        if (obj) {
                            layer.msg("注销成功")
                            location.href = "checkUser";
                        }
                    }
                });
            }
        );
    });
}
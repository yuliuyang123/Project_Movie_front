<%--
  Created by IntelliJ IDEA.
  User: aaa
  Date: 2022/3/23
  Time: 15:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<base href="<%=basePath %>" />
<html>
<head>
    <meta charset="UTF-8">
    <title>注册中心</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="static/layui/css/layui.css" media="all">
    <script src="static/layui/layui.js" charset="utf-8"></script>
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .main-body {top:50%;left:50%;position:absolute;-webkit-transform:translate(-50%,-50%);-moz-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);-o-transform:translate(-50%,-50%);transform:translate(-50%,-50%);overflow:hidden;}
        .login-main .login-bottom .center .item input {display:inline-block;width:227px;height:22px;padding:0;position:absolute;border:0;outline:0;font-size:14px;letter-spacing:0;}
        .login-main .login-bottom .tip .icon-nocheck {display:inline-block;width:10px;height:10px;border-radius:2px;border:solid 1px #9abcda;position:relative;top:2px;margin:1px 8px 1px 1px;cursor:pointer;}
        .login-main .login-bottom .tip .icon-check {margin:0 7px 0 0;width:14px;height:14px;border:none;background:url(images/icon-login.png) no-repeat -111px -48px;}
        .login-main .login-bottom .center .item .icon {display:inline-block;width:33px;height:22px;}
        .login-main .login-bottom .center .item {width:288px;height:35px;border-bottom:1px solid #dae1e6;margin-bottom:35px;}
        .login-main {width:428px;position:relative;float:left;}
        .login-main .login-top {height:117px;background-color: #e4382c;border-radius:12px 12px 0 0;font-family:SourceHanSansCN-Regular;font-size:30px;font-weight:400;font-stretch:normal;letter-spacing:0;color:#fff;line-height:117px;text-align:center;overflow:hidden;-webkit-transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0);}
        .login-main .login-top .bg1 {display:inline-block;width:74px;height:74px;background:#fff;opacity:.1;border-radius:0 74px 0 0;position:absolute;left:0;top:43px;}
        .login-main .login-top .bg2 {display:inline-block;width:94px;height:94px;background:#fff;opacity:.1;border-radius:50%;position:absolute;right:-16px;top:-16px;}
        .login-main .login-bottom {width:428px;background:#fff;border-radius:0 0 12px 12px;padding-bottom:53px;}
        .login-main .login-bottom .center {width:288px;margin:0 auto;padding-top:40px;padding-bottom:15px;position:relative;}
        .login-main .login-bottom .tip {clear:both;height:16px;line-height:16px;width:288px;margin:0 auto;}
        input::-webkit-input-placeholder {color:#a6aebf;}
        input::-moz-placeholder {/* Mozilla Firefox 19+ */            color:#a6aebf;}
        input:-moz-placeholder {/* Mozilla Firefox 4 to 18 */            color:#a6aebf;}
        input:-ms-input-placeholder {/* Internet Explorer 10-11 */            color:#a6aebf;}
        input:-webkit-autofill {/* 取消Chrome记住密码的背景颜色 */            -webkit-box-shadow:0 0 0 1000px white inset !important;}
        html {height:100%;}
        .login-main .login-bottom .tip {clear:both;height:16px;line-height:16px;width:288px;margin:0 auto;}
        .login-main .login-bottom .tip .login-tip {font-family:MicrosoftYaHei;font-size:12px;font-weight:400;font-stretch:normal;letter-spacing:0;color:#9abcda;cursor:pointer;}
        .login-main .login-bottom .tip .forget-password {font-stretch:normal;letter-spacing:0;color:#1391ff;text-decoration:none;position:absolute;right:62px;}
        .login-main .login-bottom .login-btn {width:288px;height:40px;background-color:#1E9FFF;border-radius:16px;margin:24px auto 0;text-align:center;line-height:40px;color:#fff;font-size:14px;letter-spacing:0;cursor:pointer;border:none;}
        .login-main .login-bottom .center .item .validateImg {position:absolute;right:1px;cursor:pointer;height:36px;border:1px solid #e6e6e6;}
        .footer {left:0;bottom:0;color:#fff;width:100%;position:absolute;text-align:center;line-height:30px;padding-bottom:10px;text-shadow:#000 0.1em 0.1em 0.1em;font-size:14px;}
        .padding-5 {padding:5px !important;}
        .footer a,.footer span {color:#fff;}
        @media screen and (max-width:428px) {.login-main {width:360px !important;}
            .login-main .login-top {width:360px !important;}
            .login-main .login-bottom {width:360px !important;}
        }
        body {
            background: url("static/images/R-C.jpg");
            background-size: cover;
        }
    </style>
</head>
<body>
<div class="main-body">
    <div class="login-main">
        <div class="login-top" style="background-color: #437652">
            <span>用户注册中心</span>
            <span class="bg1"></span>
            <span class="bg2"></span>
        </div>
        <form class="layui-form login-bottom">
            <div class="center">
                <div class="item">
                    <%--<input id="uid" type="hidden" name="id"/>--%>
                    <%--lay-verify="required"  自定义--%>
                    <input id="uid" type="hidden" name="lid"/>
                    <input type="text" name="phone" lay-verify="required|phone"  placeholder="请注册账号" maxlength="24"/>
                </div>
                <div class="item">
                    <input type="luname" name="luname" lay-verify="required"  placeholder="请输入用户昵称" maxlength="20">
                </div>
                <div class="item">
                    <input type="password" id="pwd" lay-verify="required|password"  placeholder="请输入密码" maxlength="20">
                </div>
                <div class="item">
                    <input type="password" id="pwd1" name="password" lay-verify="required|password"  placeholder="请确认密码" maxlength="20">
                </div>
            </div>



            <div class="layui-form-item" style="text-align:center; width:100%;height:100%;margin:0px;">
                <a  href="login.jsp" style="font-size: 15px;text-align: center"  >返回上一页</a>
            </div>

            <div class="layui-form-item" style="text-align:center; width:100%;height:100%;margin:0px;">
                <button class="login-btn" lay-submit="" lay-filter="login1" style="background-color: #437652">立即注册</button>
            </div>
        </form>
    </div>
</div>
<script>
    layui.use(['form','jquery','layer'], function () {
        var $ = layui.jquery,
            form = layui.form,
            layer = layui.layer,
            table = layui.table
            // $ = layui.$
        ;

        // // 登录过期的时候，跳出ifram框架
        // if (top.location != self.location) top.location = self.location;

        form.render();

        // 当前弹出层，防止ID被覆盖
        var parentIndex = layer.index;

        // checkUsername
        form.verify({
            phone: function (value, item) { //value：表单的值、item：表单的DOM对象
                // 验证手机号
                if(!/^1\d{10}$/.test(value)){
                    return '请输入正确的手机号';
                }
                var info;
                var id = $('#uid').val();
                $.ajax({
                    url: "checkUsername",
                    async: false,
                    data: {phone: value, id: id},
                    success: function (data) {
                        console.info(data);
                        if (data.success) {
                            info = "";
                        } else {
                            info = "账号名重复";
                        }
                    }
                });
                return info;
            },
            // 验证密码
            password: [
                /^[\S]{6,12}$/
                , '密码必须6到12位，且不能出现空格'
            ]
        });

        // 进行注册操作
        form.on('submit(login1)', function (data) {
            data = data.field;
            if (data.phone == '') {
                layer.msg('注册账号不能为空');
                return false;
            }
            if (data.password == '') {
                layer.msg('密码不能为空');
                return false;
            }

            var pwd = $("#pwd").val();
            var pwd1=$("#pwd1").val();
            if (pwd!=pwd1){
                layer.msg('密码不一致,请重新输入');
                return false;
            }


            //往后台发送ajax请求
            $.post("zhuceSave",data,function (data) {
                // alert("点击了注册")
                if (data.success){
                    layer.msg('注册成功,即将跳转到登录界面，请进行登录!', function () {
                        window.location='login.jsp';
                    });
                } else {
                    layer.msg(data.error);
                }
            });
            return false;
        });
    });
</script>
</body>
</html>

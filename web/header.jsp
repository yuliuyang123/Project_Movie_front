<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/bootstrap/css/bootstrap.css" rel="stylesheet">
    <script src="static/bootstrap/js/jquery-3.3.1.min.js"></script>
    <script src="static/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="static/css/header.css">
    <script src="static/js/header.js" charset="utf-8"></script>
    <script src="static/js/Api.js"></script>

    <script src="static/layui/layui.js" charset="utf-8"></script>
    <link rel="stylesheet" href="static/layui/css/layui.css" media="all">
    <title>淘票票电影</title>
</head>
<body>
<!-- 导航栏 -->
<div class="header navbar navbar-fixed-top">
    <div class="header-top">
        <div class="header-inner">
            <h1>
                <a href="javascript:void(0)" class="logo"></a>
            </h1>
            <div class="nav">
                <ul>
                    <li><a href="javascript:void(0)" id="shouye">首页</a></li>
                    <%--<li class="active"><a href="javascript:void(0)">电影</a></li>--%>
                    <li class="active"><a href="javascript:void(0)" onclick="writeComment()">取票</a></li>
                    <li><a href="javascript:void(0)">榜单</a></li>
                </ul>
            </div>
            <div class="app-download">
            </div>
            <div class="user-info">
                <div class="user-avatar J-login">
                    <ul class="layui-nav" style="background-color: #fff;">
                        <li class="layui-nav-item header-li" style="width:40px;" lay-unselect="" style="width: 40px;">
                        </li>
                    </ul>
                </div>
            </div>
            <%--<input name="searchMovie" class="search" type="search" maxlength="32" placeholder="找电影" autocomplete="off">--%>
            <%--<input class="submit" type="submit" value="">--%>
        </div>
    </div>
</div>

<script>
    var clientHeight = document.documentElement.clientHeight;
    $(function () {
        initHtml();
    })

    //初始化HTML
    function initHtml() {
        WriteCommentHtml =
            "<div class=\"layui-form-item\" style='height:100px;'>" +
            "<label class=\"layui-form-label commentcontenttext\">取票码</label>" +
            "<div class=\"layui-input-block commentcontent\">" +
            " <input id='comment_content_write' type=\"test\" name=\"actors\" required  lay-verify=\"required\" placeholder=\"请输入取票码\" autocomplete=\"off\" class=\"layui-input\">\n" +
            "</div>" +
            "</div>"
        ;
    }

    //写评论
    function writeComment() {
        layui.use(['laypage', 'layer', 'table'], function () {
            var laypage = layui.laypage;
            var layer = layui.layer;
            var table = layui.table
            //写评论
            layer.open({
                type: 1
                , title: "取票" //不显示标题栏
                , closeBtn: false
                , area: '430px;'
                , shade: 0.8
                , offset: clientHeight / 10
                , id: 'LAY_layuipro' //设定一个id，防止重复弹出
                , btn: ['确认取票', '取消']
                , yes: function () {
                    //评论的内容
                    var comment_content = $('#comment_content_write').val();
                    // alert(comment_content)
                    if (comment_content == "") {
                        layer.alert('取票码不能空，失败！', {icon: 0, offset: clientHeight / 10},
                            function () {
                                layer.close(layer.index);
                            }
                        );
                    } else {
                        console.log(comment_content);
                        $.ajax({
                            type: 'post',
                            url: "order/updateStateByticketId",
                            dataType: 'json',
                            data: {
                                ticket_code: comment_content
                            },
                            success: function (obj) {
                                if (obj.success) {
                                    layer.alert('取票成功！', {icon: 0, offset: clientHeight / 10},
                                        function () {
                                            layer.closeAll();
                                            location.reload();
                                        }
                                    );
                                } else {
                                    layer.alert('取票有误或订单未支付！', {icon: 0, offset: clientHeight / 10},
                                        function () {
                                            layer.closeAll();
                                        }
                                    );
                                }
                            }
                        });

                    }
                }

                , btnAlign: 'c movie-last'
                , moveType: 0 //拖拽模式，0或者1
                , content: WriteCommentHtml
            });
        });
    }

    $("#shouye").on('click', function () {
        location.href = "checkUser";
    });


    layui.use('layer', function () { //独立版的layer无需执行这一句

    });
</script>
</body>
</html>
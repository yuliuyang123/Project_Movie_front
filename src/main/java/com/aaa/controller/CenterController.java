package com.aaa.controller;

import com.aaa.dao.OrderFormDao;
import com.aaa.entity.OrderForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("center")
public class CenterController {
    @Autowired
    public OrderFormDao orderFormDao;
    @RequestMapping("toList")
    public String toList(){
        return "center";
    }

    @RequestMapping("listByLuid")
    @ResponseBody
    public List<OrderForm> listByLuid(Long luid){
        List<OrderForm> orderForms = orderFormDao.listByLuid(luid);
        return orderForms;
    }
}

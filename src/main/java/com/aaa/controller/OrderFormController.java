package com.aaa.controller;

import com.aaa.config.AlipayConfig;
import com.aaa.dao.OrderFormDao;
import com.aaa.entity.*;
import com.aaa.service.OrderFormService;
import com.aaa.service.StatisticsService;
import com.aaa.util.DefaultMsg;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


@Controller
@RequestMapping("/order")
public class OrderFormController {

    @RequestMapping("/checkUser")
    public String checkUser(){
        return "mainPage";
    }

    @Autowired
    private OrderFormService orderFormService;
    @Autowired
    private StatisticsService statisticsService;
    @Autowired
    private OrderFormDao orderFormDao;
    public String ordeId =null;
    @RequestMapping("/save")
    @ResponseBody
    public DefaultMsg save(String buy_seats,Long tid,String price,String luid,Long numseat,Long fid,Long ordeId ){
        OrderForm orderForm = new OrderForm();
        orderForm.setBuy_seats(buy_seats);
        orderForm.setOrdeId(ordeId);
        TicketSales ticketSales = new TicketSales();
        ticketSales.setTid(tid);
        orderForm.setTicketSales(ticketSales);
        orderForm.setPrice(Double.valueOf(price));
        LeadingUser user = new LeadingUser();
        user.setLid(Long.valueOf(luid));
        orderForm.setLuid(user);
        DefaultMsg defaultMsg = orderFormService.save(orderForm);
        orderFormService.update(numseat,tid);
        Statistics statistics = new Statistics();
        Film film = new Film();
        film.setId(fid);
        statistics.setFilm(film);
        statistics.setZprice(Double.valueOf(price));
        statistics.setZquantity(numseat);
        statisticsService.saveOrUpdate(statistics);
        return defaultMsg;
    }


    @RequestMapping("/toList")
    public String toList(){
        return "pay";
    }
    @RequestMapping("/listByOrdeId")
    @ResponseBody
    public OrderForm listByOrdeId(Long ordeid){
        OrderForm orderForm = orderFormDao.listByOrdeId(ordeid);
        return orderForm;
    }
    @RequestMapping("/updateState")
    @ResponseBody
    public DefaultMsg updateState(String oid){

        DefaultMsg defaultMsg = new DefaultMsg();
        Integer integer1 = orderFormDao.updateState(Long.valueOf(9), oid);
        if (integer1==0){
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }
    @RequestMapping("/updateStateByticketId")
    @ResponseBody
    public DefaultMsg updateStateByticketId(Long ticket_code){
        DefaultMsg defaultMsg = new DefaultMsg();
        Integer integer1 = orderFormDao.updateStateByticketId(Long.valueOf(1), ticket_code);
        if (integer1==0){
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }

    @RequestMapping("/pay")
    @ResponseBody
    public DefaultMsg pay(String oid, String oname, String money,  Model model) {
        this.ordeId=oid;
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);
        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayConfig.return_url);
        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = oid;
        //付款金额，必填
        String total_amount = money;
        //订单名称，必填
        String subject =oname;
        //商品描述，可空

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        //请求
        String result = null;
        DefaultMsg defultMsg = new DefaultMsg();
        try {
            result = alipayClient.pageExecute(alipayRequest).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
            defultMsg.setSuccess(0);
            defultMsg.setError(e.getMessage());
            return defultMsg;
        }
        model.addAttribute("result", result);
        defultMsg.setTarget(result);
        return defultMsg;
    }

    /**
     * 支付成功 后 支付宝自动调用
     * @return
     */
    @RequestMapping("/paySuccess")
    public String paySuccess(HttpServletRequest request) throws  Exception {
        //获取到订单的id 更改订单id的状态
        Map<String,String> params = new HashMap<String,String>();
        Map<String,String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type); //调用SDK验证签名

        //——请在这里编写您的程序（以下代码仅作参考）——
        if(signVerified) {
            orderFormDao.updateState(Long.valueOf(3),ordeId);
            //去数据库更新订单的状态
        }else {

        }
        return "mainPage";
    }

}

package com.aaa.controller;

import com.aaa.dao.CommentsDao;
import com.aaa.entity.Comments;
import com.aaa.entity.Film;
import com.aaa.entity.LeadingUser;
import com.aaa.util.DateUtil;
import com.aaa.util.DefaultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/comments")
public class CommentsController {
    @Autowired
    private CommentsDao commentsDao;

    @RequestMapping("/listData")
    @ResponseBody
    public List<Comments> listData(Long fid){
        List<Comments> tableData = commentsDao.listAll(fid);
        return tableData;
    }
    @RequestMapping("/save")
    @ResponseBody
    public DefaultMsg save(String content, Long luid, Long fid){
        DefaultMsg defaultMsg = new DefaultMsg();
        Comments comments = new Comments();
        comments.setContent(content);
        Film film = new Film();
        film.setId(fid);
        comments.setFilm(film);
        LeadingUser leadingUser = new LeadingUser();
        leadingUser.setLid(luid);
        comments.setLeadingUser(leadingUser);
        comments.setCreate_time(DateUtil.getCurrentDate());
        Integer save = commentsDao.save(comments);
        if(save==0){
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }
}

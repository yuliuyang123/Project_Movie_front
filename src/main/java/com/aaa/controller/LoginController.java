package com.aaa.controller;

import com.aaa.dao.LeadingUserDao;
import com.aaa.entity.LeadingUser;
import com.aaa.service.LeadingUserService;
import com.aaa.util.DefaultMsg;
import com.aaa.util.DefaultMsgP;
import com.aaa.util.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class LoginController {
    @Autowired
    private LeadingUserDao leadingUserDao;
    @Autowired
    private LeadingUserService leadingUserService;

    @RequestMapping("/checkUser")
    public String checkUser(){
        return "mainPage";
    }


    @RequestMapping("/session")
    @ResponseBody
    public Session<LeadingUser> session(HttpServletRequest request, HttpServletResponse response, HttpSession session ){
       Session<LeadingUser> leadingUserSession = new Session<LeadingUser>();
       List<LeadingUser> list = new ArrayList<LeadingUser>();
         LeadingUser dbUser = (LeadingUser) request.getSession().getAttribute("SESSION_USERS");
        if(dbUser==null){
            leadingUserSession.setSuccess(0);
        }else {
            list.add(dbUser);
            leadingUserSession.setData(list);
        }
        return leadingUserSession;
    }
    //去除session
    @RequestMapping("/uotSession")
    @ResponseBody
    public Integer uotSession(HttpServletRequest request, HttpServletResponse response, HttpSession session ){
        request.getSession().removeAttribute("SESSION_USERS");
        return 1;
    }

    @RequestMapping("/checkReception")
    @ResponseBody
    public DefaultMsg checkUsers(LeadingUser leadingUser, HttpSession session){
        DefaultMsg defaultMsg = new DefaultMsg();
        LeadingUser leadingUser1 = leadingUserService.checkReception(leadingUser);
        if (leadingUser1 == null) {
            defaultMsg.setError("用户不存在或被禁用?请进行注册");
            defaultMsg.setSuccess(0);
        }
        //  查询到的用户数据  传数据 SESSION_USERS
        session.setAttribute("SESSION_USERS", leadingUser1);
        session.setMaxInactiveInterval(-1);
        return defaultMsg;
    }
    /**
     * 校验用户名是否重复
     */
    @RequestMapping("/checkUsername")
    @ResponseBody
    public DefaultMsg checkUsername(String phone, Integer id){
        DefaultMsg defaultMsg = leadingUserService.checkPhone(phone,id);
        return defaultMsg;
    }
    /**
     * 根据id查询用户信息
     */
    @RequestMapping("/findByLid")
    @ResponseBody
    public LeadingUser findByLid(Long lid){
        LeadingUser leadingUser =leadingUserDao.findByLid(lid);
        return leadingUser;
    }

    /**
     * 注册添加
     */
    @RequestMapping("/zhuceSave")
    @ResponseBody
    public DefaultMsg zhuceSave(LeadingUser leadingUser){
        DefaultMsg save = leadingUserService.save(leadingUser);
        return save;
    }

    /**
     * 个人信息修改
     */
    @RequestMapping("/gerenUpdate")
    @ResponseBody
    public DefaultMsg gerenUpdate(LeadingUser leadingUser){
        DefaultMsg save = leadingUserService.save(leadingUser);
        return save;
    }
    /**
     * 密码修改
     */
    @RequestMapping("/modifyUserPwd")
    @ResponseBody
    public DefaultMsg modifyUserPwd(LeadingUser leadingUser){
        DefaultMsg defaultMsg = new DefaultMsg();
         Integer count= leadingUserDao.modifyUserPwd(leadingUser);
        System.out.println("count"+count);
        if (count==0){
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }
    /**
     * 头像昵称修改
     */
    @RequestMapping("/update")
    @ResponseBody
    public DefaultMsg update(String luname, String picture, Long lid){
        DefaultMsg defaultMsg = new DefaultMsg();
        System.out.println(luname);
        System.out.println(picture);
        System.out.println(lid);
         Integer count= leadingUserDao.update(luname,picture,lid);
        if (count==0){
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }

    /**
     * 图片上传
     * @param file
     * @return
     */
    @RequestMapping("/upload")
    @ResponseBody
    public DefaultMsgP upload(MultipartFile file){
        String saveName = UUID.randomUUID().toString();
        File saveFile = new File("Q:/upload/"+saveName);
        DefaultMsgP defaultMsg = new DefaultMsgP();
        try {
            file.transferTo(saveFile);
            defaultMsg.setTarget(saveName);
        } catch (IOException e) {
            defaultMsg.setError("上传文件失败");
            e.printStackTrace();
        }
        return  defaultMsg;
    }
}

package com.aaa.controller;

import com.aaa.dao.OrderFormDao;
import com.aaa.dao.SellTicketDao;
import com.aaa.entity.OrderForm;
import com.aaa.entity.TicketSales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

//购票控制器
@Controller
@RequestMapping("/sell")
public class SellController {
    @Autowired
    private SellTicketDao sellTicketDao;
//    @Autowired
//    private SellTicketService sellTicketService;
    @Autowired
    private OrderFormDao orderFormDao;
    @RequestMapping("/toList")
    public String toList(){
        return "sell/list";
    }


    @RequestMapping("/toBuyTickets")
    public  String toBuyTickets(){
        return "sell/toBuyTickets";
    }

    @RequestMapping("/buyTickets")
    public  String buyTickets(){
        return "sell/buyTickets";
    }
//获取所有电影
@RequestMapping("/listAll")
@ResponseBody
public   List<TicketSales> listAll(){
    List<TicketSales> ticketSales = sellTicketDao.listAll();
    return ticketSales;
}
    /**
     * 获取购买电影数据
     * @return
     */
    @RequestMapping("/listData")
    @ResponseBody
    public TicketSales listDate(Long  fid){
        TicketSales ticketSales = sellTicketDao.listDate(fid);
        return  ticketSales;
    }

    @RequestMapping("/listChangci")
    @ResponseBody
    public List<TicketSales> listChangci(Long  fid){
        List<TicketSales> ticketSales = sellTicketDao.listChangci(fid);
        return  ticketSales;
    }

    /**
     * 获取id对象的场次数据/
     * @return
     */
    @RequestMapping("/findTicketSalesByTId")
    @ResponseBody
    public TicketSales findTicketSalesByTId(Long  tid){
        TicketSales page1 = sellTicketDao.findTicketSalesByTId(tid);
        List<OrderForm> orderForms = orderFormDao.listByTid(tid);
        page1.setOrderList(orderForms);
        return  page1;
    }

}

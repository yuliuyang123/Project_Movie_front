package com.aaa.dao;

import com.aaa.entity.Film;
import com.aaa.entity.Ftype;
import com.aaa.entity.Statistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class StatisticsDao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Statistics> rowMapper=new RowMapper<Statistics>() {
        public Statistics mapRow(ResultSet resultSet, int i) throws SQLException {
            Statistics statistics=new Statistics();
            statistics.setSid(resultSet.getLong("sid"));
            statistics.setZprice(resultSet.getDouble("zprice"));
            statistics.setZquantity(resultSet.getLong("zquantity"));
            Film film = new Film();
            film.setId(resultSet.getLong("id"));
            film.setChinese_name(resultSet.getString("chinese_name"));
            Ftype ftype=new Ftype();
            ftype.setTid(resultSet.getLong("tid"));
            ftype.setName(resultSet.getString("name"));
            film.setFtype(ftype);
            statistics.setFilm(film);
            return statistics;
        }
    };

    //查询当前电影是否已经有了
    public Integer listByFid(Long fid){
        String sql = "select count(sid) from statistics where fid = ?";
        Integer integer = this.jdbcTemplate.queryForObject(sql, Integer.class, fid);
        return integer;
    }

    public Integer save(Statistics statistics){
        String sql ="insert into statistics(fid,zquantity,zprice) values(?,?,?)";
        int update = this.jdbcTemplate.update(sql, statistics.getFilm().getId(), statistics.getZquantity(), statistics.getZprice());
        return update;
    }
    public Integer update(Statistics statistics){
        String sql = "update statistics set  zquantity=(zquantity+?),zprice=(zprice+?) where fid = ?";
        int update = this.jdbcTemplate.update(sql, statistics.getZquantity(), statistics.getZprice(), statistics.getFilm().getId());
        return update;
    }

}

package com.aaa.dao;

import com.aaa.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class OrderFormDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private RowMapper<OrderForm> rowMapper = new RowMapper<OrderForm>() {

        public OrderForm mapRow(ResultSet resultSet, int i) throws SQLException {
            OrderForm orderForm = new OrderForm();
            orderForm.setOid(resultSet.getLong("oid"));
            orderForm.setOrdeId(resultSet.getLong("ordeId"));
            orderForm.setTicket_code(resultSet.getLong("ticket_code"));
            orderForm.setBooking_time(resultSet.getString("booking_time"));
            orderForm.setBuy_seats(resultSet.getString("buy_seats"));
            orderForm.setPrice(resultSet.getDouble("price"));
            TicketSales ticketSales = new TicketSales();
            ticketSales.setTid(resultSet.getLong("tid"));
            ticketSales.setOpening_time(resultSet.getString("opening_time"));
            ticketSales.setBreak_time(resultSet.getString("break_time"));
            Film film=new Film();
            film.setId(resultSet.getLong("fid"));
            film.setChinese_name(resultSet.getString("chinese_name"));
            film.setImgUrl(resultSet.getString("img_url"));
            film.setDuration(resultSet.getString("duration"));
            film.setCountry(resultSet.getString("country"));
//            Ftype ftype=new Ftype();
//            ftype.setTid(resultSet.getLong("tid"));
//            ftype.setName(resultSet.getString("name"));
//            film.setFtype(ftype);
            ticketSales.setFilm(film);
            Hall hall = new Hall();
            hall.setHid(resultSet.getLong("hid"));
            hall.setHname(resultSet.getString("hname"));
            ticketSales.setHall(hall);
            orderForm.setTicketSales(ticketSales);
            Identity identity = new Identity();
            identity.setId(resultSet.getLong("id"));
            identity.setIname(resultSet.getString("iname"));
            orderForm.setState(identity);
            LeadingUser user = new LeadingUser();
            user.setLid(resultSet.getLong("lid"));
            user.setLuname(resultSet.getString("luname"));
            orderForm.setLuid(user);
            return orderForm;
        }
    };
    //查询当前场次的所有订单
    public List<OrderForm> listByTid(Long tid){
        String sql = "select * from order_form o join ticket_sales t on o.tid =t.tid  join film f on t.fid = f.id join hall h on h.hid=t.hid join leading_user u on o.luid = u.lid join identity i on o.state=i.id where o.tid = ? and o.state !=9";
        List<OrderForm> query = this.jdbcTemplate.query(sql, rowMapper, tid);
        return query;
    }
    //查询当前用户的所有订单
    public List<OrderForm> listByLuid(Long luid){
        String sql = "select * from order_form o join ticket_sales t on o.tid =t.tid  join film f on t.fid = f.id join hall h on h.hid=t.hid join leading_user u on o.luid = u.lid join identity i on o.state=i.id where o.luid = ? and o.state!=8  ORDER BY o.booking_time desc";
        List<OrderForm> query = this.jdbcTemplate.query(sql, rowMapper, luid);
        return query;
    }
    //查询当前订单号的订单
    public OrderForm listByOrdeId(Long ordeId){
        String sql = "  select * from order_form o join ticket_sales t on o.tid =t.tid  join film f on t.fid = f.id join hall h on h.hid=t.hid join leading_user u on o.luid = u.lid join identity i on o.state=i.id where o.ordeid = ? ";
        OrderForm query = this.jdbcTemplate.queryForObject(sql, rowMapper, ordeId);
        return query;
    }
    //添加订单
    public Integer save(OrderForm orderForm){
        String sql="insert into order_form(ordeId,ticket_code,buy_seats,booking_time,state,price,tid,luid) values(?,?,?,?,?,?,?,?)";
        int count = jdbcTemplate.update(sql,orderForm.getOrdeId(),orderForm.getTicket_code(),orderForm.getBuy_seats(),orderForm.getBooking_time(),orderForm.getState().getId(),orderForm.getPrice(),
                orderForm.getTicketSales().getTid(),orderForm.getLuid().getLid());
        return count;
    }
    //修改剩余票数
    public Integer update(Long numseat,Long tid){
        String sql = "update ticket_sales set  remaining_votes=(remaining_votes-?) where tid = ?";
        int update = this.jdbcTemplate.update(sql, numseat, tid);
        return update;
    }
    //根据编号修改订单状态
    public Integer updateState(Long state,String ordeId){
        String sql = "update order_form set state = ? where ordeId = ?";
        int update = this.jdbcTemplate.update(sql,state,ordeId);
        return update;
    }
    //根据取票码修改订单状态
    public Integer updateStateByticketId(Long state,Long ticket_code){
        String sql = "update order_form set state = ? where ticket_code = ?";
        int update = this.jdbcTemplate.update(sql,state,ticket_code);
        return update;
    }
}

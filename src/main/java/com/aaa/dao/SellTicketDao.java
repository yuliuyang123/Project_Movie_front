package com.aaa.dao;

import com.aaa.entity.Film;
import com.aaa.entity.Ftype;
import com.aaa.entity.Hall;
import com.aaa.entity.TicketSales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class SellTicketDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<TicketSales> rowMapper = new RowMapper<TicketSales>() {
        public TicketSales mapRow(ResultSet resultSet, int i) throws SQLException {
            TicketSales ticketSales = new TicketSales();
            ticketSales.setTid(resultSet.getLong("tid"));
            ticketSales.setOpening_time(resultSet.getString("opening_time"));
            ticketSales.setBreak_time(resultSet.getString("break_time"));
            ticketSales.setCreation_time(resultSet.getString("creation_time"));
            ticketSales.setRemaining_votes(resultSet.getString("remaining_votes"));
            ticketSales.setPrice(resultSet.getDouble("price"));
            Film film=new Film();
            film.setId(resultSet.getLong("fid"));
            film.setChinese_name(resultSet.getString("chinese_name"));
            film.setEnglish_name(resultSet.getString("english_name"));
            film.setImgUrl(resultSet.getString("img_url"));
            film.setDuration(resultSet.getString("duration"));
            film.setShowtimes(resultSet.getString("showtimes"));
            film.setIntroduction(resultSet.getString("introduction"));
            film.setCountry(resultSet.getString("country"));
            film.setDirector(resultSet.getString("director"));
            film.setActors(resultSet.getString("actors"));
            Ftype ftype=new Ftype();
            ftype.setTid(resultSet.getLong("tid"));
            ftype.setName(resultSet.getString("name"));
            film.setFtype(ftype);
            ticketSales.setFilm(film);
            Hall hall = new Hall();
            hall.setHid(resultSet.getLong("hid"));
            hall.setHname(resultSet.getString("hname"));
            hall.setHang(resultSet.getString("hang"));
            hall.setSeat(resultSet.getString("seat"));
            hall.setLie(resultSet.getString("lie"));
            hall.setCancel(resultSet.getString("cancel"));
            hall.setDetailed(resultSet.getString("detailed"));
            ticketSales.setHall(hall);
            return ticketSales;
        }
    };
    //去重查询拍过的单个电影
    public TicketSales listDate(Long fid){
        String sql = "select * from ticket_sales t join film f on t.fid =f.id join ftype ft on f.tid = ft.tid join hall h on h.hid = t.hid join state st on t.sid =st.sid where t.tid IN ( select max(tid) from ticket_sales GROUP BY fid)and t.fid =?";
       TicketSales result = this.jdbcTemplate.queryForObject(sql, rowMapper,fid);
        return  result;
    }
    //去重查询拍过的多电影
    public List<TicketSales> listAll(){
        String sql = "select * from ticket_sales t join film f on t.fid =f.id join ftype ft on f.tid = ft.tid join hall h on h.hid = t.hid join state st on t.sid =st.sid where t.tid IN ( select max(tid) from ticket_sales GROUP BY fid) and opening_time>NOW()";
        List<TicketSales> result = this.jdbcTemplate.query(sql, rowMapper);
        return  result;
    }



    //根据电影id查询场次
    public List<TicketSales> listChangci(Long fid){
        String sql = "select * from ticket_sales t join film f on t.fid =f.id join ftype ft on f.tid = ft.tid join hall h on h.hid = t.hid\n" +
                "join state st on t.sid =st.sid where t.fid =? and opening_time>NOW() order by opening_time  desc ";
        List<TicketSales> result = this.jdbcTemplate.query(sql, rowMapper,fid);
        return  result;
    }

    /**
     * 查询总条数
     * @return
     */
    public Integer count(Long fid){
        String sql = "select count(tid) from ticket_sales where fid = ? ";
        Integer integer = this.jdbcTemplate.queryForObject(sql, Integer.class,fid);
        return  integer;
    }
    //
    public TicketSales findTicketSalesByTId(Long tid){
        String sql = "select * from ticket_sales t join film f on t.fid =f.id join ftype ft on f.tid = ft.tid join hall h on h.hid = t.hid\n" +
                "join state st on t.sid =st.sid where t.tid =?  ";
        TicketSales ticketSales = this.jdbcTemplate.queryForObject(sql, rowMapper, tid);
        return ticketSales;
    }

}

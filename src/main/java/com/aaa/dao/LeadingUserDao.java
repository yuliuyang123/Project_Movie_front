package com.aaa.dao;

import com.aaa.entity.Identity;
import com.aaa.entity.LeadingUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class LeadingUserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private RowMapper<LeadingUser> rowMapper=new RowMapper<LeadingUser>() {
        public LeadingUser mapRow(ResultSet resultSet, int i) throws SQLException {
            LeadingUser leadingUserDao=new LeadingUser();
            leadingUserDao.setLid(resultSet.getLong("lid"));
            leadingUserDao.setLuname(resultSet.getString("luname"));
            leadingUserDao.setPassword(resultSet.getString("password"));
            leadingUserDao.setPhone(resultSet.getString("phone"));
            leadingUserDao.setPicture(resultSet.getString("picture"));
            leadingUserDao.setCreate_time(resultSet.getString("create_time"));
            Identity identity = new Identity();
            identity.setId(resultSet.getLong("id"));
            identity.setIname(resultSet.getString("iname"));
            leadingUserDao.setIdentity(identity);
            return leadingUserDao;
        }
    };
    //判断用户名和密码是否为空
    public LeadingUser findByPhone(LeadingUser leadingUser) {
        String sql="select * from leading_user lu join identity i on lu.id = i.id  where phone=? and password=? and lu.id = 6 ";
        List<LeadingUser> result = this.jdbcTemplate.query(sql, rowMapper, leadingUser.getPhone(),leadingUser.getPassword());
        if (result.size()>0){
            return result.get(0);
        }else {
            return null;
        }
    }
    //根据查询用户的个人信息
    public LeadingUser findByLid(Long lid) {
        String sql="select * from leading_user lu join identity i on lu.id = i.id where lu.lid =? ";
        List<LeadingUser> result = this.jdbcTemplate.query(sql, rowMapper, lid);
        if (result.size()>0){
            return result.get(0);
        }else {
            return null;
        }
    }


    //前台用户的注册gerenUpdate
    public Integer save(LeadingUser leadingUser) {
        String sql="insert into leading_user(phone,luname,password,create_time,id) values(?,?,?,?,?)";
        int count = this.jdbcTemplate.update(sql, leadingUser.getPhone(),leadingUser.getLuname(),leadingUser.getPassword(), leadingUser.getCreate_time(),6);
        return count;
    }

    //前台用户的修改个人信息
    public Integer update(String luname, String picture,Long lid) {
        String sql="update leading_user set picture=?,luname=? where lid=? ";
        int count = this.jdbcTemplate.update(sql,picture,luname,lid);
        return count;
    }
    //前台用户的修改密码
    public Integer modifyUserPwd(LeadingUser leadingUser) {
        String sql="update leading_user set password=? where lid=? ";
        int count = this.jdbcTemplate.update(sql, leadingUser.getPassword(),leadingUser.getLid());
        return count;
    }

    //根据用户名查询用户对象
    public Integer countByPhone(String phone) {
        String sql="select count(lid) as c from leading_user where phone=?";
        Integer result = this.jdbcTemplate.queryForObject(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("c");
            }
        }, phone);
        return result;
    }
    //根据用户名和id查询对象
    public Integer countByPhoneId(String phone, Integer id) {
        String sql="select count(lid) as c from leading_user where phone=? and lid!=?";
        Integer result = this.jdbcTemplate.queryForObject(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
                return resultSet.getInt("c");
            }
        }, phone, id);
        return result;
    }

}

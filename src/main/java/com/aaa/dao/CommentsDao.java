package com.aaa.dao;

import com.aaa.entity.Comments;
import com.aaa.entity.Film;
import com.aaa.entity.LeadingUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CommentsDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private RowMapper<Comments> rowMapper=new RowMapper<Comments>() {
        public Comments mapRow(ResultSet resultSet, int i) throws SQLException {
            Comments comments=new Comments();
            comments.setId(resultSet.getLong("id"));
            comments.setContent(resultSet.getString("content"));
            comments.setCreate_time(resultSet.getString("create_time"));
            Film film=new Film();
            film.setChinese_name(resultSet.getString("chinese_name"));
            comments.setFilm(film);
            LeadingUser leadingUser=new LeadingUser();
            leadingUser.setLuname(resultSet.getString("luname"));
            leadingUser.setPicture(resultSet.getString("picture"));
            comments.setLeadingUser(leadingUser);
            return comments;
        }
    };


    public List<Comments> listAll(Long fid){
        String sql="select * from comments c  join film f on c.fid =f.id join leading_user lu on c.uid = lu.lid  where fid =?  order by c.create_time desc  ";
        List<Comments> result = jdbcTemplate.query(sql, rowMapper,fid);
        return result;
    }


    //添加用户评论
    public Integer save(Comments comments) {
        String sql="insert into comments(content,uid,fid,create_time) values(?,?,?,?)";
        int count = this.jdbcTemplate.update(sql, comments.getContent(),comments.getLeadingUser().getLid(),comments.getFilm().getId(),comments.getCreate_time());
        return count;
    }

}

package com.aaa.util;

public class DefaultMsgP {
    private Integer success=1;
    private String error;

    private Object target;

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.setSuccess(0);
        this.error = error;
    }
}

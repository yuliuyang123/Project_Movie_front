package com.aaa.util;

import java.util.List;

public class Session<T> {
    private Integer success = 1;
    private List<T> data;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}

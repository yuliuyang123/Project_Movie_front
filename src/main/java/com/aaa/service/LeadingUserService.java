package com.aaa.service;


import com.aaa.entity.LeadingUser;
import com.aaa.util.DefaultMsg;

public interface LeadingUserService {
     LeadingUser checkReception(LeadingUser leadingUser);
     DefaultMsg save(LeadingUser leadingUser);
     DefaultMsg checkPhone(String phone, Integer id);

}

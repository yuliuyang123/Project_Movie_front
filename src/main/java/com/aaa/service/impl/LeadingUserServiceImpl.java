package com.aaa.service.impl;

import com.aaa.dao.LeadingUserDao;
import com.aaa.entity.LeadingUser;
import com.aaa.service.LeadingUserService;
import com.aaa.util.DateUtil;
import com.aaa.util.DefaultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LeadingUserServiceImpl implements LeadingUserService {
    @Autowired
    public LeadingUserDao leadingUserDao;

    public LeadingUser checkReception(LeadingUser leadingUser) {
        return leadingUserDao.findByPhone(leadingUser);
    }

    public DefaultMsg save(LeadingUser leadingUser) {
        DefaultMsg defaultMsg = new DefaultMsg();
        Integer count;
        leadingUser.setCreate_time(DateUtil.getCurrentDate());
        count = leadingUserDao.save(leadingUser);

        if (count == 0) {
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }

    public DefaultMsg checkPhone(String phone, Integer id) {
        DefaultMsg defaultMsg = new DefaultMsg();
        Integer count = leadingUserDao.countByPhone(phone);
        if (count > 0) {
            defaultMsg.setSuccess(0);
        }
        return defaultMsg;
    }
}

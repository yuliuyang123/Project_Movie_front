package com.aaa.service.impl;

import com.aaa.dao.OrderFormDao;
import com.aaa.entity.Identity;
import com.aaa.entity.OrderForm;
import com.aaa.service.OrderFormService;
import com.aaa.util.DefaultMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class OrderFormServiceImpl implements OrderFormService {
    @Autowired
    private OrderFormDao orderFormDao;
    public DefaultMsg save(OrderForm orderForm) {
        DefaultMsg defaultMsg = new DefaultMsg();
        Integer count;
        int hashCode = java.util.UUID.randomUUID().toString().hashCode();
        if (hashCode <0){
            hashCode=-hashCode;
        }
        String ticket_code = String.format("%010d", hashCode).substring(0,8);
        orderForm.setTicket_code(Long.valueOf(ticket_code));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        orderForm.setBooking_time(simpleDateFormat.format(new Date()));
        Identity state = new Identity();
        state.setId(Long.valueOf(2));
        orderForm.setState(state);
        count = orderFormDao.save(orderForm);
//        count = hallDao.save(hall);
        if (count == 0) {
            // defaultMsg.setSuccess(0);
            defaultMsg.setError("操作失败");
        }
        return defaultMsg;
    }

    public Integer update(Long numseat, Long tid) {
        return orderFormDao.update(numseat,tid);
    }
}

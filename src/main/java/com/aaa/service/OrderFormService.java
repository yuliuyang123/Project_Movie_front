package com.aaa.service;

import com.aaa.entity.OrderForm;
import com.aaa.util.DefaultMsg;

public interface OrderFormService {
    DefaultMsg save(OrderForm orderForm);
    Integer update(Long numseat, Long tid);

}

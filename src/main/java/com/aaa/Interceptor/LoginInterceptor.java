package com.aaa.Interceptor;

import com.aaa.entity.LeadingUser;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginInterceptor implements HandlerInterceptor {
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //获取session
        HttpSession session = request.getSession();

        //获取session中的用户对象
        LeadingUser user = (LeadingUser)session.getAttribute("SESSION_USERS");
        //判断user 是否为null
        if(user==null){
            response.sendRedirect("http://localhost:8083/Project_Movie_front_Web_exploded/login.jsp");
            return false;
        }else {
            return true;
        }

    }
}

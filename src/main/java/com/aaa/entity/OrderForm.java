package com.aaa.entity;

public class OrderForm {
    private Long oid;           //id
    private Long ordeId;        //订单编号
    private Long ticket_code;   //取票码
    private String booking_time;//购票时间
    private Identity state;//状态
    private Double price;//价钱
    private TicketSales ticketSales;//所属场次
    private String buy_seats;//买的座位
    private LeadingUser luid;//用户

    public String getBuy_seats() {
        return buy_seats;
    }

    public void setBuy_seats(String buy_seats) {
        this.buy_seats = buy_seats;
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public Long getOrdeId() {
        return ordeId;
    }

    public void setOrdeId(Long ordeId) {
        this.ordeId = ordeId;
    }

    public Long getTicket_code() {
        return ticket_code;
    }

    public void setTicket_code(Long ticket_code) {
        this.ticket_code = ticket_code;
    }

    public String getBooking_time() {
        return booking_time;
    }

    public void setBooking_time(String booking_time) {
        this.booking_time = booking_time;
    }

    public Identity getState() {
        return state;
    }

    public void setState(Identity state) {
        this.state = state;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public TicketSales getTicketSales() {
        return ticketSales;
    }

    public void setTicketSales(TicketSales ticketSales) {
        this.ticketSales = ticketSales;
    }


    public LeadingUser getLuid() {
        return luid;
    }

    public void setLuid(LeadingUser luid) {
        this.luid = luid;
    }
}

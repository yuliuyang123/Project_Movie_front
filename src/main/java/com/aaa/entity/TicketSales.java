package com.aaa.entity;

import java.util.List;

//售票（拍片）
public class TicketSales {
    private Long tid;                   //拍片id
    private String opening_time;        //开始时间
    private String break_time;          //结束时间
    private Double price;               //价钱
    private Film film;
    private Hall hall;
    private String remaining_votes;
    private Long sid;
    private String creation_time;
    private List<OrderForm> orderList; //所有的订单集合

    public List<OrderForm> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderForm> orderList) {
        this.orderList = orderList;
    }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public String getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(String creation_time) {
        this.creation_time = creation_time;
    }

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public String getOpening_time() {
        return opening_time;
    }

    public void setOpening_time(String opening_time) {
        this.opening_time = opening_time;
    }

    public String getBreak_time() {
        return break_time;
    }

    public void setBreak_time(String break_time) {
        this.break_time = break_time;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public String getRemaining_votes() {
        return remaining_votes;
    }

    public void setRemaining_votes(String remaining_votes) {
        this.remaining_votes = remaining_votes;
    }


}

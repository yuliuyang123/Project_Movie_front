package com.aaa.config;
import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {

//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000119647201";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQDWk05bNUjLrhXUT/vwkSdQPAr/TlkaQ90w1rvUE4HceJOP2mUuLNlRAA900VA072vVJJCNLwNaZa95pMJ/J+d9C3x91tQSHqf3k8gBVF82r8IQqOGavfwjsNN1u84lgCKniN+1FNIjL4Sk6J5bCQNDuMsn2FlqFHcJQZ48+ZdonKhy2df3p1/0W6B3OYI4hswTg1gfz1iYngF2IDBvPwxn0N5vS6EGTHxINdIg9vR6+rmuPT+3ov25ieaCxv1qPGTDPdkFiwZ5ch1Xe4dEtFQZd4k9EFHfV8vat0IWpNuuixr1Esq5QqYjoWBnRuybvZOvWm37uknqYpiH+BD9S+zLAgMBAAECggEBAJee7T5Lz42FDI3Cmu7pUpNCai7vSMzjaVfKVFzOD/QfC34Zv2c6b7DpzwInqXpON68bsDzjHIZy1eSpxHmfU9arGCO/eoj3Oo8Z0LkHymPVG9czzn7Ogm0h1Lobo9Yhzdr+P70v4jDUsfDpIoSHQNRIrt0P5lar8VUMFelWIGPdCqDM9sawUOLQqAz8f7awYgaHXT+v5MgbO+Q1vR/FH/xVaAhTVeDK04CZVnK/aL1nfanrRGtowVClJGLaOFwcJyVvdLdV2y15hedn73s7DkhTug5zfFrxrzDQGMkMHEh6V5PuIUAvBhicxA05MoWgupsELkREoLKqr/an659fWEkCgYEA+9uKbu7PTSmtsd1VG9Pwndu7Q4du/OGzBmi4hlQkCmvNPhp17bZYCR8+K3wHLQ6ZHrBIGp/HytlqYPwRJqKt5eO2JNxj0ZDWdYoRAZZOcIXtgC3uP2JqsTB8n5Pbg/E45FrENeQXuRdAOlck/q127pEPI7KMOO4dlvLi+HfhHR0CgYEA2hrJbxs2nfXiSKIrk2EhjPVrmMsj1vlknAy/x8ALqFl8gcHNY/6jbyTwNFFJVjb79znPfyzSw6/a22qDhALgKoRTDxlP4SUxI2ZReDPucNr9V/MMJLTIHtunb2tnPVK20Y2tSZlpwbGQnVqEXSa3X1Pj2kdlHBK49bNa1ien1QcCgYEAplQBo2thSlNZG5rRRzR7trTTgjBiEu23PmdQXZ4ZwywYeogdE6UVlMimVS9uycnXQ5YluDeHSaa2A2DUrtBGygjNVKCb512g1DUfMA4ktVktVTaAHioPzf74fqa2FPfsm5p59CEzrY/2YdEFZJAIeFsiRW/3/Oz8a6DyVidNat0CgYEArJdn8YEybjuGbn3OR9kkKJyJIacW3GYMJKS+Gn6xeu4dZb0MIGX/xNMRNgVhKFVofFIGDhhVTL9bTCh0nEF+TIbcB2W7XWoGAYJVU+6ccDgLJM7TPDTG7Axb6gKu9sr+msMnbsd09e7ihnJ7rg8WKtyCwVef/mAed2gPqMpGuH8CgYEAh8pYsntKXhSUyO72QLn3WPVUkpJgydMPyKDXgU/0TKYjryBW/MiJa/jA3XxeQnxf2yGaeLZR69LLOnwhxs44rJVCNEhQAQRJaXrWtQ9LbKHoiZOzqqnLdTTgxp9h0X5QE9T3v9rqdZdoJ+YoVWMETB/hjDsXAAWGlCCzpYlP7A8=";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl9BQMdufXKCb5Ph7DtIoFcUvH2vPSPnoNFnqtivm32xP3B/2/nuSDtWyy12aE2ED9pXAFHRYl8e1D4GegBdyWYQ2ejw2aKBunw6FC1Zdf3Q5WsaLsPvJa9VTzeY8WNSuxaoqqHBdj3AnL7BDT7yOYsKdOOHy20heENVStdVrKpzlsgNZIuzNdBJs9OGg2XAxJ32EARb91QRR5Jz6AOhTEmGdg0t12rfa40Q17Han+zpisZC9Bwk1HNwI4keGelS86RJzSBRuvtjCL0XkD63pPwe7QsW0fkgdp3sTQKOjXsLVxvT/7C1iFYn3NKnrfrGRocOzWJYLvuajfj49RDRu7QIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://工程公网访问地址/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://localhost:8083/Project_Movie_front_Web_exploded/order/paySuccess";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

